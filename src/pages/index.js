import React from "react"

import Layout from "../components/layout"

import SEO from "../components/seo"

import styled from "styled-components"

import { Colors } from "../utils/ui/colors"

const Wrapper = styled.div`
  height: 100vh;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const SuperTitle = styled.h4 `
  margin-bottom: 5px;
  font-size: 16px;
`

const Title = styled.h1 `
  font-size: 48px;
  line-height: 1;
  margin-bottom: 20px;
  color: ${Colors.primary};
`

const Footer = styled.footer `
  text-align: center;
`

const GatsbyLink = styled.a `
  color: ${Colors.primary};
`

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`web`, `kit`, `react`]} />
    <Wrapper>

      <SuperTitle>thanks for using</SuperTitle>
      <Title>J-Web-Kit</Title>

      <Footer>
        © {new Date().getFullYear()}, Built with
        {` `}
        <GatsbyLink href="https://www.gatsbyjs.org">Gatsby</GatsbyLink>
      </Footer>
    </Wrapper>
  </Layout>
)

export default IndexPage
