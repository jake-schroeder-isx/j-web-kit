/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import React from "react"
import PropTypes from "prop-types"

import styled from "styled-components"


import "./reset.css"
import "./global.css"

const Wrapper = styled.div `
    margin: 0 auto;
    max-width: 960px;
`





const Layout = ({ children }) => (


    <Wrapper>
      
      <main>{children}</main>

    </Wrapper>

)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
