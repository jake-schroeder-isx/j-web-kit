import { Colors } from "./src/utils/ui/colors"

module.exports = {
  siteMetadata: {
    title: `J-Web-Kit`,
    description: `This is a react framework for building data driven progressive websites that are reliable, secure, blazing fast and most importantly extremely cheap to host.`,
    author: `jake.schroeder@isophex.com`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: ``,
        short_name: ``,
        start_url: `/`,
        background_color: Colors.primary,
        theme_color: Colors.primary,
        display: `minimal-ui`,
        icon: `src/images/j-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline

    // `gatsby-plugin-offline`,
  ],
}
